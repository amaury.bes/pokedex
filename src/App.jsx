import { useEffect, useState } from "react";
import axios from "axios";
import Header from "./components/Header";
import List from "./components/List";
import "./App.css";

function App() {
  const [pokemons, setPokemons] = useState([]);
  const [filter, setFilter] = useState('');
  useEffect(() => {
    if (pokemons.length === 0) {
      axios
        .get("https://pokeapi.co/api/v2/pokemon/?limit=151")
        .then((response) => {
          response.data.results.forEach((pokemon) => {
            axios.get(pokemon.url).then((response) => {
              setPokemons((oldArray) => [...oldArray, response.data]);
            });
          });
        });
    }
  }, []);

  return (
    <div className="App">
      <Header pokemons={pokemons} filter={filter} setFilter={setFilter}/>
      <List pokemons={pokemons} filter={filter}/>
    </div>
  );
}

export default App;
