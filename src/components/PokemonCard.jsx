import * as React from "react";
import {
  Card,
  CardActionArea,
  CardContent,
  Box,
  Typography,
  Avatar,
} from "@mui/material";

const PokemonCard = ({ pokemon }) => {
  const [expanded, setExpanded] = React.useState(false);
  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card
      style={{
        width: "90%",
        margin: 10,
        backgroundColor: "#FAFAFA",
        border: "none",
      }}
    >
      <CardActionArea onClick={handleExpandClick}>
        <CardContent>
          <Avatar
            src={pokemon.sprites.front_default}
            variant="square"
            style={{ float: "left", width: 60, height: 60, marginRight: 10, marginBottom: 10 }}
          />
          <Typography
            variant="h5"
            component="div"
            style={{ float: "left" }}
          >
            {pokemon.name}
          </Typography>
          <Typography
            variant="h5"
            component="div"
            style={{ float: "right" }}
          >
            {pokemon.id}
          </Typography>
          {expanded && (
            <>
              <Box
                style={{
                  width: "90%",
                  display: "flex",
                  alignItems: "center",
                  paddingTop: 10
                }}
              >
                <Typography
                  variant="body2"
                  component="div"
                  style={{ float: "right" }}
                >
                  Type
                </Typography>
                {pokemon.types.map((type) => (
                  <Typography
                    variant="body2"
                    component="div"
                    style={{
                      marginLeft: 10,
                      padding: 2,
                      backgroundColor: "rgba(42, 16, 109, 0.2)",
                    }}
                  >
                    {type.type.name}
                  </Typography>
                ))}
              </Box>
              <Box
                style={{
                  width: "90%",
                  display: "flex",
                  alignItems: "center",
                  paddingTop: 10,
                  marginLeft: 60
                }}
              >
                <Typography
                  variant="body2"
                  component="div"
                  style={{ float: "right" }}
                >
                  Height
                </Typography>
                <Typography
                  variant="body2"
                  component="div"
                  style={{ float: "right" }}
                >
                  {pokemon.height}
                </Typography>
              </Box>
              <Box
                style={{
                  width: "90%",
                  display: "flex",
                  alignItems: "center",
                  paddingTop: 10,
                  marginLeft: 60
                }}
              >
                <Typography
                  variant="body2"
                  component="div"
                  style={{ float: "right" }}
                >
                  Weight
                </Typography>
                <Typography
                  variant="body2"
                  component="div"
                  style={{ float: "right" }}
                >
                  {pokemon.weight}
                </Typography>
              </Box>
            </>
          )}
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default PokemonCard;
