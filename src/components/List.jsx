import PokemonCard from "./PokemonCard";

const List = ({ pokemons, filter }) => (
  <div
    style={{
      marginTop: 120,
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    }}
  >
    {pokemons?.filter((pokemon) => pokemon.name.includes(filter)).map((pokemon) => (
      <PokemonCard pokemon={pokemon} />
    ))}
  </div>
);
export default List;
