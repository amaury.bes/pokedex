import { TextField, Typography, styled, Card, Box } from "@mui/material";

const Header = ({ pokemons, setFilter, filter }) => {
  const total = pokemons.length;

  const returnTypes = (types, acc) => {
    return types
      .map((type) => {
        if (!acc.includes(type.type.name)) {
          return type.type.name;
        }
      })
      .filter((val) => val !== undefined);
  };

  const numberOfTypes = pokemons.reduce(
    (acc, current) => [...acc, ...returnTypes(current.types, acc)],
    []
  ).length;

  const onChangeInput= (e) => {
    setFilter(e.target.value);
  }

  const CssTextField = styled(TextField)({
    "#outlined-search": {
      color: "white",
    },
    "#outlined-search-label": {
      color: "white",
    },
    "& label.Mui-focused": {
      color: "white",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "white",
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "white",
      },
      "&:hover fieldset": {
        borderColor: "white",
      },
      "&.Mui-focused fieldset": {
        borderColor: "white",
      },
    },
  });

  return (
    <Box
      style={{
        width: "100%",
        height: 263,
        backgroundColor: " #2A106D",
        fontFamily: "Inter",
        color: "white",
      }}
    >
      <Typography
        gutterBottom
        variant="h5"
        component="div"
        style={{ float: "left", marginTop: 32, marginLeft: 40 }}
      >
        Pokedex
      </Typography>
      <Typography
        gutterBottom
        variant="h5"
        color="white"
        component="div"
        style={{
          float: "right",
          marginTop: 32,
          marginRight: 40,
          color: "white",
        }}
      >
        Première génération - Kanto
      </Typography>
      <Box
        style={{
          display: "flex",
          justifyContent: "center",
          paddingTop: 100,
          width: "100%",
        }}
      >
        <CssTextField
          id="outlined-search"
          label="Chercher un pokémon"
          type="search"
          style={{ width: "90%" }}
          value={filter}
          autoFocus
          onChange={onChangeInput}
        />
      </Box>

      <Box style={{ display: "flex", justifyContent: "center" }}>
        <Card style={{ width: 256, margin: 5 }}>
          <Typography gutterBottom variant="h5" component="div">
            Nombre total de Pokémon
          </Typography>
          <Typography gutterBottom variant="h5" component="div">
            {total}
          </Typography>
        </Card>
        <Card style={{ width: 256, margin: 5 }}>
          <Typography gutterBottom variant="h5" component="div">
            Types totaux
          </Typography>
          <Typography gutterBottom variant="h5" component="div">
            {numberOfTypes}
          </Typography>
        </Card>
      </Box>
    </Box>
  );
};
export default Header;
