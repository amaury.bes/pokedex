Bonjour,

Je n'ai pas eu le temps d'aller jusqu'a faire le détail d'un pokemon :)
Il y a aussi encore beaucoup d'amélioration à faire mais j'ai tenté de faire a l'essentiel dans 
le temps imparti.

Pour démarrer il suffit de lancer depuis la racine les commandes :
npm i
npm start
et ensuite aller à cette url : http://localhost:3000

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
